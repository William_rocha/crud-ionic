import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { CadastroProvider } from '../../providers/cadastro/cadastro';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  nome: string;
  nomeSalvo: any;
  id: number;
  constructor(public navCtrl: NavController, private cadastro: CadastroProvider) {

  }


    public salvar(){
      this.cadastro.inserir(this.nome);
    }

    buscar(){
      this.nomeSalvo = this.cadastro.buscar(this.id);
    }
}
