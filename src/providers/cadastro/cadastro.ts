import { Injectable } from '@angular/core';
import { BancoProvider } from '../banco/banco';
import { ThrowStmt } from '@angular/compiler';
import { SQLiteObject } from '@ionic-native/sqlite';

/*
  Generated class for the CadastroProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class CadastroProvider {

  constructor(private banco: BancoProvider) {
    console.log('Hello CadastroProvider Provider');
  }

  public inserir(nome){
    return this.banco.getBanco().then((db:SQLiteObject) => {
      let sql = 'INSERT INTO cadastro (nome) values( ?)';//O ? Evita o SQLInjection
      let parametros = [nome];
      return db.executeSql(sql, parametros);
    });
  }

  public buscar(id: number){
    return this.banco.getBanco().then((db:SQLiteObject) => {//no banco aponto para o banco de dados
      let sql = 'SELECT * from cadastro where id = ? ';
      let parametros = [id];//pega o id
      return db.executeSql(sql, parametros)//manda executar a query como parametro, vai retornar alguns dados
        .then((data: any) => {//dara vai armazenar os dados
          if(data.rows.length > 0)//se voltou um resultado
            return data.rows.item(0).nome;//Vai pegar o nome
          return null;//cado contrario vai retornar null
        })
    })
  }
}
