import { Injectable } from '@angular/core';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';

@Injectable()
export class BancoProvider {

  constructor(private sqlite: SQLite) {
    console.log('Hello BancoProvider Provider');
  }

  public getBanco() {
    return this.sqlite.create({
      name: 'data.db',
      location: 'default'
    });
  }

  public criarBanco(){
    return this.getBanco().then((db: SQLiteObject) => {//Caso o banco não exista, será criado
      this.criarTabela(db);
    }).catch(e => console.log(e));
  }

  private criarTabela(db: SQLiteObject){
    db.sqlBatch([
      'CREATE TABLE IF NOT EXISTS cadastro ('+
        'id integer primary key AUTOINCREMENT NOT NULL,'+
        'nome TEXT)'
    ]).then(() => console.log('Tabela criada'))
      .catch(e => console.error('Erro ao criar a tabela', e));
  }
}
